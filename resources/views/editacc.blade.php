<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Account</title>
</head>
<body>
    <h1>Edit Account</h1>
    <form action="/edit" method="POST">
        @csrf
        <label for="username">Username : </label>
        <input type="text" id="username" name="username">

        <label for="email">Email : </label>
        <input type="email" id="email" name="email">

        <label for="password">Password : </label>
        <input type="password" id="password" name="password">

        <input type="submit" value="Update">
    </form>
</body>
</html>