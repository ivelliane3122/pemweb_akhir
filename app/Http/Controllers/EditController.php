<?php

namespace App\Http\Controllers;

use App\Models\EditModel;
use Illuminate\Http\Request;

class EditController extends Controller
{
    public function editForm() {
        return view('editacc');
    }

    public function store(Request $request){
        $edit = new EditModel();
        $edit->username = $request->input('username');
        $edit->email = $request->input('email');
        $edit->password = $request->input('password');
        $edit->save();

        return redirect('/');
    }
}
